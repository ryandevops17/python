## [1.3.2](https://gitlab.com/just-ci/tests/python/compare/v1.3.1...v1.3.2) (2022-08-03)


### Bug Fixes

* try this ([761660e](https://gitlab.com/just-ci/tests/python/commit/761660e686224c9915d63edd0da3859bf6db7248))
